# Managing "my_chose_software"

[[_TOC_]]

## What is this software you have chosen?

A few lines describing it, what is its purpose? who are the developers? how old
is it? increasing in popularity? (google trends?) Are the any repositories for
this software?

## Security History

Examine the history of your chosen software (CVE databases etc), any
vulnerabilities in the past? how frequently new releases? how frequently
patches? involved in any major incidents? etc maybe you can generate a
graph/figure? Any other bugs? is it easy to submit a bug report? 

## Practice

How do you do the following:
    * Install, verify completed installation
    * Configure
    * Update/Patch
    * Remove, verify completed removal

## Conclusion and Reflection

What are the future challenges for this software? Would you like to be responsible for maintaining it in production for 10000 users?

Briefly reflect over your choice of software and work process, everything have gone according to
plan? what was harder or easier than expected?

(remember to link to sources DIRECTLY IN THE TEXT, e.g. if you have read about [Backup on
Wikipedia](https://en.wikipedia.org/w/index.php?title=Backup&oldid=998232746),
also not how I just linked to the static Wikipedia-page and not the dynamic one)
# Portfolio 3: Maintaining Software

## Table of Contents

[[_TOC_]]

## Background

See lecture notes of Software Package Management. The goal of this assignment is
for you to learn the importance of maintaining the software on a computer, more
than just installing it. Vulnerabilities in software comes in all shapes and
sizes and we need to be prepared for them in the software we are maintaining.

## What should be handed in in Blackboard?

A PDF-document (900 words +/-10%). No git repository this time, just a report.
But feel free to use a git repo and write in Markdown if you would like so.

## Rules

* This is an individual assignment, but you are allowed to discuss with other
  students.
* If you have received significant input/ideas from others, you must mention it
  in the report
* [No cheating](https://innsida.ntnu.no/wiki/-/wiki/English/Cheating+on+exams)
  (we will check for plagiarism)

## The Assignment

1. Choose a Windows-based server-software or client-software. You can choose
   from the following list or choose something else based on your own interest.
    * MS Exchange
    * MS DNS
    * MS Edge
    * MS Sharepoint
    * MS Internet Explorer
    * MS FTP server
    * OpenSSH server
    * FileZilla
    * Adobe Flash
    * Adobe Acrobat Reader
    * Oracle Java
    * Google Chrome
    * Mozille Firefox
    * Mozilla Thunderbird
    * Visual Studio Code
    * VLC media player
    * Blender
    * Gimp
    * Audacity
    * Opera
    * TeamViewer
1. Practical work (document in your report the commands from your PowerShell
   session, we only expect a total of somewhere between 3 and 20 command lines),
   how do you do the following:
    * Install, verify completed installation
    * Configure
    * Update/Patch
    * Remove, verify completed removal
1. Theoretical work:
    * Examine the history of your chosen software (CVE databases etc), any
      vulnerabilities in the past? how frequently new releases? how frequently
      patches? involved in any major incidents? etc maybe you can generate a
      graph/figure?
    * What options do you have for maintaining it? is it supported by a package
      management solution ala Chocolatey/WinGet?
    * If your chosen software gets a new vulnerability and the vulnerability
      gets exploited, how can you detect it? can you configure increased
      logging? etc
1. Write your individual report in whatever tool/format/markup-language you
   prefer _as long as it can generate a PDF with working hyperlinks_
    * justify/explain your design
    * use links directly in the text to cite your sources (you do not need a
      list of sources in the end of the document)
    * reflect over your solution and work process

See [p3-tpl.md](./p3-tpl.md) for a brief template you can start with if you want to.

## Grading and feedback

* You will be graded based on the quality of your report (incl PowerShell
  command lines): language, use of sources, justification, technology
  understanding, reflection
* You will receive an "approximate grade" (as explained in the course
  description and in the lectures) in Blackboard and a paragraph justifying your
  grade ("automatisk begrunnelse")
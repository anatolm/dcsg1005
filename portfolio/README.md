# Title, something like "Ransomware Protection with PowerShell" or whatever you prefer

THE GIT REPO FOR THIS PORTFOLIO IS: https://gitlab.stud.idi.ntnu.no/USERNAME/REPO

[[_TOC_]]

## Problem description / Goal of this project

What is the problem you are trying to solve, be as specific as you can

## Design / Solution

Justify/explain your design

## Discussion, incl security aspects

How does your solution compare to others you find on the Internet, what are
benefits, negative sides, what should have been done differently? Think
security: can this solution be misused somehow?

## Conclusion and Reflection

reflect over your solution and work process, everything have gone according to
plan? what do you achieve or not achieve? what took more time than you expected

(remember to link to sources, e.g. if you have read about [Backup on
Wikipedia](https://en.wikipedia.org/w/index.php?title=Backup&oldid=998232746),
also not how I just linked to the static Wikipedia-page and not the dynamic one)
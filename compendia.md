[[_TOC_]]

# Introduction and tools

## Cloud Computing

##### Cloud Computing

  - SaaS

  - PaaS

  - IaaS

Let’s see what the *public clouds* AWS, Azure and Google has to offer

##### Our Key Aspect

  - Self-service\!

  - It’s dynamic\!

  - (A measured service)

## Security and Privacy

##### Security and Privacy

  - Privacy: storing data on someone else’s computers

  - All issues related to outsourcing (possibly loosing control)

  - Otherwise mostly same issues as on-premise
    (patch/update,backups,access control,logs,monitoring)

## OpenStack

##### Cloud Computing

  - [OpenStack software](https://www.openstack.org/software)

  - [Openstack at NTNU](https://www.ntnu.no/wiki/display/skyhigh)

  - [Using the
    webinterface](https://www.ntnu.no/wiki/display/skyhigh/Using+the+webinterface)

## Review questions and problems

1.  What is a *security group* in OpenStack? What do you use it for?

## Lab tutorials

1.  Do the [Basic Infrastructure
    Orchestration](https://gitlab.com/erikhje/dcsg1005/-/blob/master/heat-labs.md)
    exercise with the template  
    [single\_windows\_Srv2019.yaml](https://gitlab.com/erikhje/heat-mono/-/blob/master/single_windows_Srv2019.yaml).
    You have completed the exercise when you have logged in to the host
    and deleted the stack after you have logged out.

2.  Do the [Basic Infrastructure
    Orchestration](https://gitlab.com/erikhje/dcsg1005/-/blob/master/heat-labs.md)
    exercise with the template
    [cl\_dc\_srv\_basic.yaml](https://gitlab.com/erikhje/heat-mono/-/blob/master/cl_dc_srv_basic.yaml).
    You have completed the exercise when you have logged in to the all
    the hosts at the same time (you should have three remote desktop
    windows up at the same time).

3.  Read the first paragraph and do the commands in the first "box" in
    [Getting Started with
    PowerShell](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#getting-started-with-powershell).
    Now you can install software with the `choco` command. Go on and
    install with `choco`
    
      - `vscode` (after installing, in vscode, install the extensions
        `markdownlint`, `powershell` and `rewrap`)
    
      - `git`
    
      - (also `pandoc` and `miktex` if you want to generate pdf from
        markdown on the command line)

4.  Study the video [SSH Key for NTNU sin
    GITLAB](https://www.youtube.com/watch?v=GWD6Rf51qdc) and do the
    same:
    
    1.  Create a private git repo at <https://gitlab.stud.idi.ntnu.no>
    
    2.  For creating a ssh keypair and put the public key on in the
        clipboard:
        
        ``` 
            ssh-keygen -t rsa
            Get-Content $HOME\.ssh\id_rsa.pub | Set-Clipboard
        ```
    
    3.  Instead of cloning by https, you can clone with
        
        ``` 
            git clone git@gitlab.stud.idi.ntnu.no:YOUR_NTNU_username/dcsg1005.git
        ```

5.  (**EXTRA**) You will be creating, deleting, recreating
    infrastructures many times this semester. That should be the work
    habit when we use a cloud. Can you create your own way of saving
    time when you get a new IP and password every time? maybe a "mstsc
    command line template" or a desktop shotcut you can easily edit?
    feel free to share your solution with the rest of the class when you
    come up with something that works for you.

# Windows Server and PowerShell 1

## Windows Server 2019

##### Windows Server 2019

  - See [lesson from Tor
    Ivar](https://ntnu.blackboard.com/bbcswebdav/pid-1230441-dt-content-rid-33368006_1/xid-33368006_1)

## Review questions and problems

1.  Describe the Heat component of OpenStack (in other words: What is
    OpenStack Heat?).

## Lab tutorials

1.  Study and work through all exercises included in [PowerShell 1:
    Standalone
    Host](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-1-standalone-host).

# Windows Server and PowerShell 2

## PowerShell

##### PowerShell

  - See [separate document on
    PowerShell](https://gitlab.com/erikhje/dcsg1005/blob/master/powershell.md)

## Review questions and problems

1.  What is *splatting*?

## Lab tutorials

1.  Study and work through all exercises included in [PowerShell 2:
    Domain-joined hosts and
    Remoting](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#powershell-2-domain-joined-hosts-and-remoting).

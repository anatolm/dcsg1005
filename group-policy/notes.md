# Active Directory

### Active Directory

  - Forest  
    domains share structure, schema, global catalog, trust

  - Domain  
    network-wide id, auth, trust, replicate

  - OU  
    organizational units

  - Container  
    like an OU, but cannot have GPOs

  - Group  
    have users, used for access control (groups have SID, OUs don’t)

  - RootDSE  
    root directory server agent service entry (“root of the directory
    information tree”)

See [Well-known users and groups and built-in
accounts](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/delegating-administration-of-default-containers-and-ous#well-known-users-and-groups-and-built-in-accounts)

The most important part about active directory is how to design the set
and structure of Organizational Units (OUs). The best practice for
designing OUs state that OUs should be structured primarely to
facilitate administrative delegation, secondary to facilite GPOs. In
practice this means that if you have have multiple teams of system
administrator, you should delegate responsibility to those teams by
letting them be administrators for a set of OUs. This is closely related
to the secondary purpose: facilitate GPOs. This means that you should
group users and computers in OUs based on who should have the same
policies applied to them. E.g. in a University you would have a natural
separation between students, faculty and administrative staff. And the
same for computers: students workstations should be in a separate OU
from faculty laptops.

Sites are simple. The concept of site is relevant when your
infrastructure consists of multiple physical sites where you need to
consider that bandwidth between sites might be an issue. If you have
sites that are far apart (e.g. a branch office in a different part of
the world) you will probably want users at those sites to have some
local services instead of communicating with the main physical site for
all computing services. AD sites are just for this purpose, so that you
e.g. can create a Read-Only Domain Controller (RODC) at a low-bandwidth
site to better serve your users at that site.

Read about forests and domains at [Understanding the Active Directory
Logical
Model](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/understanding-the-active-directory-logical-model)

Read about default containers, OUs, groups and users at [Delegating
Administration of Default Containers and
OUs](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/delegating-administration-of-default-containers-and-ous)

# Group Policy

See also [Tor Ivars video from 01:11:30
until 01:29:00](https://studntnu-my.sharepoint.com/:v:/r/personal/melling_ntnu_no/Documents/Recordings/Andre%20forelesning%20DCST_DCSG%201005%20-%20Windows%20Server%202019-20210118_081929-Opptak%20av%20m%C3%B8te.mp4?csf=1&web=1&e=kYRzHF)

### Group Policy

  - Policy settings for Computer and for Users
    
      - Computer  
        applies to all users
    
      - Users  
        only for specific users/user groups

  - Policy settings grouped into Objects (Group Policy Objects - GPOs)

## Policy Settings

### Available Settings

  - Software settings (installations)

  - Windows settings (login scripts, folder redirection, printers, ...)

  - Administrative templates

<span class="alert">Almost everything is changes to the Windows Registry
(HKLM and HKCU)</span>

Settings are grouped into Software settings, Windows settings and
Administrative templates for Computer and for Users. Some settings does
not make sense to set for specific users (or user groups) so they are
not present in the Users category only in the Computer category,
likewise the other way around. When you set something in the Computer
category it will apply to all users.

Administrative templates for Computer are changes in the HKLM part of
the registry, while Administrative templates for User are changes in
HKCU.

If a computer is not joined to a domain, you can use *Local Group
Policy* on that standalone computer if you want to manage it with the
mechanisms of group policy.

DEMO WITH LOCAL GROUP POLICY: disable task manager after CTRL-ALT-DEL:

    User configuration
     -> Administrative templates
     -> System
     -> CTRL-ALT-DEL options

(Note how this is only available to Users, and not under Computer
configuration.)

DEMO WITH LOCAL GROUP POLICY: allow bginfo.exe from sysinternals:

    choco install -y bginfo zoomit
    User configuration
     -> Administrative templates
     -> Desktop
     -> Desktop
     -> Desktop wallpaper

How can we do this in PowerShell? by using e.g.

``` 
  Set-ItemProperty -Path `
  HKLM:\Software\Policies\Microsoft\Windows\WindowsUpdate `
  -Name DisableWindowsUpdateAccess -Value 1
```

and looking up the right entry in Registry from the spreadsheet
available from [Group Policy Settings Reference Spreadsheet for Windows
10 May 2020 Update
(2004)](https://www.microsoft.com/en-us/download/101451).

### Options for Each Setting

  - Not configured

  - Enabled

  - Disabled, can mean
    
      - Reverse the setting from a previous level
    
      - Force disabling of an OS default

### Group Policy in a Domain

  - GP Settings vs GP Preferences
    
      - Settings are enforced
    
      - Preferences can be changed by the user after they have been
        applied

  - Pull-based model
    
      - Ordinary hosts: every 90 minutes (30 min random offset
        (splaytime))
    
      - Domain Controllers: every 5 minutes

The agent pulling is the Group Policy Service activated by the registry
key  
`HKLM\SYSTEM\CurrentControlSet\services\gpsvc`. A Group Policy update
can also be forced by running `Invoke-GPUpdate` (if you want it to
happen immediately you do  
`Invoke-GPUpdate -RandomDelayInMinutes 0`).

DEMO “Force disabling of an OS default” (windows has a firewall on by
default, this is not something turned on by Group Policy, but we are
going to use Group Policy to turn it off):

1.  Remember that GPOs cannot be applied to containers like Users and
    Computers, so let’s move the host CL1 into the IT OU:
    
        # As a domain administrator 
        
        # Create an OU "workstations"
        New-ADOrganizationalUnit "Workstations" -Description "Computers in fixed locations"
        
        # Move cl1 to OU workstations
        $MHT1 = @{
            Identity   = 'CN=CL1,CN=Computers,DC=sec,DC=core'
            TargetPath = 'OU=workstations,DC=sec,DC=core'
        }
        Move-ADObject @MHT1

2.  Creating a Group Policy Object is probably best to do with a GUI. We
    can do it with PowerShell if we know the exact registry settings we
    want to apply, but probably we should take advantage of a GUI in the
    design stage of a GPO:
    
        # On DC1
        
        gpme.msc
        # Create a new GPO called 'MyFWSettings'
        Computer configuration
         -> Administrative templates
         -> Network
         -> Network connections
         -> Windows Defender firewall
         -> Domain profile
         -> Protect all network connections (disable)
        # exit gpme

3.  A good strategy would be to have a repository of all GPOs and
    separate those from the ones we actually apply, just import them
    when needed. In other words, create a GPOs, label them starting with
    ’My’ and let those be our "code" which we could put in a git-repo,
    or at least have version controlled and backed-up somewhere.
    
        # On DC1
        
        # copy the one we created to one that we are going to use
        Copy-GPO -SourceName "MyFWSettings" -TargetName "FWSettings"
        
        # link it to the Workstations OU so it will be applied to CL1
        Get-GPO -Name "FWSettings" | 
         New-GPLink -Target "OU=workstations,DC=sec,DC=core"
        
        # btw we can view all GPOs with
        Get-GPO -All -Domain $env:USERDNSDOMAIN
        # or
        Get-GPO -All | Format-Table -Property displayname
        
        # Remember also that GPOs are just objects in an AD LDAP tree:
        Get-ADObject -LDAPFilter "(ObjectClass=groupPolicyContainer)" | 
         ForEach-Object {Get-GPO -Id $_.Name}
        
        # And they are just a file structure made available to hosts through a share
        Get-ChildItem C:\Windows\SYSVOL\domain\Policies
        Get-SmbShare

4.  Let’s see how this GPO affects CL1. Keep the Firewall control panel
    (`firewall.cpl`) visible along side PowerShell.
    
        # On CL1
        
        # Install if not present:
        Get-WindowsCapability -Online `
         -Name Rsat.GroupPolicy.Management.Tools~~~~0.0.1.0 |
         Add-WindowsCapability -Online
        
        Invoke-GPUpdate -RandomDelayInMinutes 0
        
        # or just
        gpupdate /force
        
        # We can view report with
        Get-GPOReport -All -Domain $env:USERDNSDOMAIN -ReportType HTML `
         -Path ".\GPOReport1.html"
        .\GPOReport1.html
        
        # or
        gpresult.exe /h GPOReport.html

(another examples is to turn of Shutdown event tracker on Windows
Servers.)

### Are we all doing the same?

  - Everyone are managing Windows servers and laptops, isn’t there some
    common best practice we can all adopt and adapt?

  - [Microsoft Security
    Baselines](https://techcommunity.microsoft.com/t5/microsoft-security-baselines/bg-p/Microsoft-Security-Baselines)

See also examples of how you can import Group Policy templates for other
applications at e.g. [Group Policy Administrative Templates
Catalog](https://admx.help/) (but please inspect what you are
downloading and applying on your domain controllers. Always look for
direct sources and consider if the code is trustworthy).

    choco install -y 7zip wget
    wget https://download.microsoft.com/download/8/5/C/85C25433-A1B0-4FFA-9429-7E023E7DA8D8/Windows%2010%20Version%2020H2%20and%20Windows%20Server%20Version%2020H2%20Security%20Baseline.zip
    7z x '.\Windows 10 Version 20H2 and Windows Server Version 20H2 Security Baseline.zip'
    cd '.\Windows-10-Windows Server-v20H2-Security-Baseline-FINAL\'
    Get-ChildItem
    cd Scripts
    .\Baseline-ADImport.ps1
    Get-GPO -All | Format-Table -Property displayname
    
    # We need to OU distinguished name several times
    $OU = "OU=Workstations,DC=sec,DC=core"
    
    # Get all currently linked to OU Workstations
    Get-ADOrganizationalUnit $OU | 
     Select-Object -ExpandProperty LinkedGroupPolicyObjects
    # if you want to see the names of the GPOs
    # from https://community.spiceworks.com/topic/2197327-powershell-script-to-get-gpo-linked-to-ou-and-its-child-ou
    $LinkedGPOs = Get-ADOrganizationalUnit $OU | 
     Select-object -ExpandProperty LinkedGroupPolicyObjects
    $LinkedGPOGUIDs = $LinkedGPOs | ForEach-object{$_.Substring(4,36)}
    $LinkedGPOGUIDs | 
     ForEach-object {Get-GPO -Guid $_ | Select-object Displayname }
    
    # link two new ones to OU Workstations
    Get-GPO -Name "MSFT Windows 10 20H2 - Computer" | 
     New-GPLink -Target $OU
    Get-GPO -Name "MSFT Windows 10 20H2 - User" | 
     New-GPLink -Target $OU
    
    # Apply
    Invoke-GPUpdate -RandomDelayInMinutes 0
    # or 
    gpupdate /force
    
    # See changes in report, Policies, Settings, Windows Settings,
    # Security Settings, Who is the "Winning GPO"?
    gpresult.exe /h GPOReport.html
    ./GPOReport.html

## Processing Order

### GPO Processing Order

When a host is joined to a domain:

1.  Local GPO

2.  GPO linked to site (do not do this)

3.  GPO linked to domain

4.  GPO linked to OU (do this)

GPO’s are created, tested, then linked to site, domain or OU.

<span class="alert">Last writer wins\! In other words, OU-GPOs
overwrites conflicting GPOs from previous steps (unless `Enforced` is
set).</span>

Remember that site is a defined IP subnet, typically a geographical
location.

<http://technet.microsoft.com/en-us/library/cc754948%28v=ws.10%29.aspx>:

![image](../inheritance.png)

## Tools

### Group Policy Tools

  - Local GPO: `gpedit.msc`

  - AD: `gpmc.msc`, `gpme.msc`  

  - PowerShell:  
    `Import-Module GroupPolicy`  
    `Get-Command -Module GroupPolicy`

  - Group Policy Results  
    `Get-GPResultantSetOfPolicy`  
    <span class="alert">Resultant Set of Policy - RSoP</span>

Remember manual client pull with `Invoke-GPUpdate`

### Some Important Notes

  - Settings for a set of users or computers, managed by the same
    administrators: <span class="alert">should be in same GPO</span>

  - Filtering by WMI

  - Enforce settings with a new GPO at the domain level to avoid changes
    by OU-administrators

  - Link order can be manipulated

  - <span class="alert">Max 999 GPOs</span>

<!-- end list -->

  - GPOs cannot be applied to the default categories Users and Computers

  - Higher level policies (domain) can be set to "enforced", meaning the
    can not be overridden by lower level (OU) (Enforced takes precedence
    over Block policy inheritance)

  - Settings that apply to the same set of users or computers and are
    managed by the same administrators, should be in the same GPO

  - A GPO (not individual policy settings) can be filtered by security
    (e.g. instead of all authenticated users, choose a specific user
    group) or WMI to only apply to a subset of users or computers

  - Use WMI filters only when necessary (exceptions), can be time
    consuming or do not time out at all (long logon times...) (WMI
    filters can be though of as conditional statements)

  - Max 999 GPOs can be applied, if you have more, NONE WILL BE
    APPLIED\!

  - Do not change default domain or domain controller policy, instead
    create a new GPO and set it to Enforce

  - You create Enforce settings at the Domain level to avoid changes by
    OU-administrators

  - You can manipulate the link order of GPOs (e.g. the GPOs linked to
    an OU)

### Great Book\!

![image](../GPbook.png)

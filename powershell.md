**An Interactive PowerShell Tutorial** (inspired by and somewhat aligned with [PowerShell 101](https://docs.microsoft.com/en-us/powershell/scripting/learn/ps101/00-introduction))

[[_TOC_]]

# PowerShell 1: Standalone Host

This first part of the tutorial assumes you are working on a standalone Windows
Server 2019 (you can probably also do this in a Windows 10).

## Windows Introduction and Background

Windows have GUI (the [Windows
Shell](https://en.wikipedia.org/wiki/Windows_shell)), the old command line
interface cmd and the preferred command line interface PowerShell (cmd and
PowerShell can also both be used in [Windows
Terminal](https://github.com/microsoft/terminal)).

To copy and paste on the command line:
* copy: mark the text and hit Enter
* paste: right click

If you are using a Windows with a different keyboard layout than what you are
used to (e.g. if you have downloaded from [Get a Windows 10 development
environment](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines))


```powershell
Get-WinUserLanguageList
Set-WinUserLanguageList -LanguageList nb-NO
```

## Getting Started with PowerShell

By default, Windows has the old Windows PowerShell (PowerShell 5.1) installed.
We want the new cross-platform version of PowerShell called PowerShell Core.
Note that the Windows PowerShell executable file is called `powershell.exe`
while the PowerShell core executable file is called `pwsh.exe` (you can also
installed PowerShell core on Linux and Mac and start it with the command
`pwsh`).
```powershell
# search powershell, right click, Run as administrator

Set-ExecutionPolicy Bypass -Scope Process -Force
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco install -y powershell-core
exit
```

PowerShell has
[ExecutionPolicies](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies)
which are meant to help you protect you from yourself, e.g. unintentional
execution of downloaded scripts. Sometimes we have to change the execution
policy to be allowed to execute our scripts. Use `Get-ExecutionPolicy` to see
which policy is set on the system, and use `Set-ExecutionPolicy` to change it.
We set this to `RemoteSigned` to require scripts downloaded from the Internet to
be digitally signed by a trusted publisher. The default for Windows clients is
`Restricted`, for servers it is `RemoteSigned`. If we have downloaded a script
that is not digitally signed by a trusted publisher, we can still execute it if
we manually `Unblock-File`.

The Chocolatey package manager (for installing software on Windows) is very
cool, but make sure you know which risk you are taking (think [supply chain
security](https://github.blog/2020-09-02-secure-your-software-supply-chain-and-protect-against-supply-chain-threats-github-blog/)).
Read [Rigorous Moderation Process for Community
Packages](https://chocolatey.org/docs/security#rigorous-moderation-process-for-community-packages).
You can use Chocolatey to install most of the software you need (like we just
did with PowerShell Core). You can check for outdated programs you have
installed with Chocolatey and upgrade them with

```powershell
# search pwsh, right click, Run as administrator
# you might have to run Set-ExecutionPolicy RemoteSigned
choco outdated
choco upgrade all
```

> Use `choco` to install the package `autoruns`. Launch `autoruns` after
> installation, what is the purpose of this software? What is
> [Sysinternals](https://docs.microsoft.com/en-us/sysinternals/)?

**Before we continue, make sure you are NOT USING PowerShell AS ADMINISTRATOR
unless explicitely told so, we only use "run as administrator" when we need to
do system administration tasks like installing software, adding users, stopping
important services, etc** (Remember: humans make mistakes, computers do not.
When we humans make mistakes we save ourselves of so much trouble if we are a
user with low privileges.)

## Cmdlets, Parameters, Aliases and Help

PowerShell commands are called *cmdlets* (pronounced “commandlets”) and have the
syntax `verb-noun`, e.g. `Write-Output`. Fortunately most of the cmdlets [have
aliases corresponding to the commands you might know from DOS (cmd.exe) or
Unix/Linux](https://docs.microsoft.com/en-us/powershell/scripting/samples/appendix-1---compatibility-aliases).
In addition there is also a short PowerShell alias to most cmdlets. To find the
cmdlet to a command you know from before you can use the cmdlet `Get-Alias`:

```powershell
# is there a cmdlet corresponding to Unix/Linux ls?
Get-Alias ls
# are there many aliases to Get-ChildItem?
Get-Alias -Definition Get-ChildItem
# list all aliases
Get-Alias
# or list them from the "alias drive" (similar to your C:\ drive)
Get-ChildItem Alias:\
``` 

Note: Aliases are nice, but get used to using the full cmdlet names, especially
in your scripts. This makes your scripts more readable and easier for others to
understand.

These cmdlets has several parameters/options that are specific to each one, but
there are also a set of [common
parameters](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_commonparameters#long-description).
Note particularly the options `-Verbose`, `-ErrorAction SilentlyContinue` and
`-WhatIf`.

> Try `Get-Service | Stop-Service -WhatIf`. Try `Get-ChildItem -Recurse
> C:\Windows\System32\wbem\MO*` with and without the option `-ErrorAction
> SilentlyContinue`.

Btw, a running program is called [a _process_ and a process most of the times
have
_threads_](https://docs.microsoft.com/en-us/windows/win32/procthread/processes-and-threads).
[A service is a special
process](https://docs.microsoft.com/en-us/windows/win32/services/services)
wrapped around another process with the purpose of controlling all service
processes in the same way. Think of a service as something that is running on
your computer without anyone being logged in (sometimes a process or a group of
processes is [wrapped in a
job](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_jobs)
but this is not as common as a service).

To get help with the cmdlets, use the cmdlet `Get-Help`, e.g. `Get-Help
Write-Output | more`. A nice feature is that you can view the help page in your
browser (on the internet) by adding the parameter `-Online`, e.g. `Get-Help
Write-Output -Online`. If you don't want to go online, you can use `Get-Help
Write-Output -Examples` or `Get-Help Write-Output -Full`.

Note that you can use TAB-completion on both commands and parameters.

> Use `Get-Help Select-String -online` (note: `Select-String` is similar to
> Linux `grep`) and find the example code for "Find a string in subdirectories".
> Try it. Did you get an access error (red text)? If so, repeat the command with
> the option `-ErrorAction SilentlyContinue` added to `Get-ChildItem`. Searching
> file systems is slow, how slow? Repeat the command but put braces (`{ }`)
> around it and prepend it with the cmdlet `Measure-Command`.

To find which cmdlets you can use, you have the cmdlet `Get-Command`

> Try `Get-Command *DNS*`

(and if you want to have some fun, repeat `Get-Command | Get-Random | Get-Help`
a couple of times)

## Drives, Profiles, Variables and Namespaces

A PowerShell drive is a data store which you can access like a file system. This
might seem a bit strange at first, but it is very practical and allows us to
reuse the same set of commands for accessing different kinds of data.

> `Get-PSDrive`, navigate (`cd`) to all drives you did not know about, and do
> `Get-ChildItem` and/or `Get-ChildItem -Recurse` to explore what is there. E.g.
> do `Get-ChildItem -Path Cert:\LocalMachine\CA` to see the Certificate
> Authorities on Local Machine.

You can create variables in PowerShell which by default will be in the
`variable` _namespace_. In other words, PowerShell has namespaces. Sometimes you
have to specify namespace and sometimes you don't:

```powershell
$a = 'heisan'
Write-Output $a
Write-Output $variable:a
cd $env:homepath
$env:homepath
$homepath
$home
$env:home
$profile
Get-Content $profile # Get-Content is same as Linux cat
```

[Environment
variables](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_environment_variables)
are in the namespace `env`. A
[Profile](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles)
is basically a script that is executed when you start PowerShell and can be used
to modify your environment. 

## Objects and Get-Member

The "Power" in PowerShell comes from piping objects instead of just a
bytestream. E.g. if you want to lookup the IP address of a fqdn (Fully Qualified
Domain Name) you can use the old-style `nslookup` program or the new PowerShell
cmdlet `Resolve-DnsName`

```powershell
nslookup.exe ftp.uninett.no
nslookup.exe ftp.uninett.no | Get-Member
# notice the top line "TypeName: System.String", the command only outputs a string

Resolve-DnsName ftp.uninett.no
Resolve-DnsName ftp.uninett.no | Get-Member
# again, notice the TypeNames, to get the IPv4 address we can now simply do
(Resolve-DnsName ftp.uninett.no).IP4Address
# very hard to do the same with nslookup, 
# would need to do some Select-String RegExp magic, maybe with some head/tail
```

When piping objects you can use `Get-Member` to see which properties and methods
the object contains. To access a property or method of an object you can use the
syntax `Object.Property` or `(Cmdlet-that-returns-object).Property` as shown
above.

> Do `$name = 'mysil'`. Use the properties and methods of the `$name`-object to
> * find out how many characters the string contains
> * print the string in upper case

## Select-Object

You can create a new object in the pipeline with a reduced set of
properties/methods with `Select-Object`

```powershell
# create a file
Write-Output mysil > abc.txt
# compare the output of
Get-ChildItem .\abc.txt | Get-Member
# with
Get-ChildItem .\abc.txt | Select-Object -Property Name,LastWriteTime | Get-Member
```

And you can use `Select-Object` similar to head and tail in Unix/Linux (which is something you will probably do quite frequently)

```powershell
Get-Process | Select-Object -Last 5
Get-ChildItem | Select-Object -First 3
```

> Do `Get-Content C:\Windows\System32\drivers\etc\services` and pipe this to
> `Select-Object` in such a way that you only output lines 30 to 50

Advanced: sometimes we also use `Select-Object` to "create new properties on the
fly". E.g. if we want to show the owner of files this is not directly available
as a property, but we can do a trick with a hash table (see also
[Appendix](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#variable-array-and-hashtable)):

```powershell
Get-ChildItem | 
  Select-Object Name,Directory,@{Name="Owner";Expression={(Get-ACL $_.Fullname).Owner}},CreationTime,LastAccessTime |
    Format-Table
```

## Where-Object, Where, ?

Objects in the pipeline can be addressed with the [automatic
variable](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_automatic_variables)
`$_` and `Where-Object` is used to filter objects similar to Unix/Linux `grep`

```powershell
Get-Service
Get-Service | Where-Object {$_.Status -eq 'Running'}
# -eq is 'exactly equal' while -match matches against a regular expression:
Get-Service | Where-Object {$_.Status -eq 'Running' -and $_.StartupType -match '.*auto.*'}
```

There is a plethora of comparison operaters you can use, browse the web page
[About Comparison
Operators](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_comparison_operators)
to see the options you have.

> Use `Get-Process` and `Where-Object` to
> * list all processes with the property Name equals 'pwsh'
> * list all processes with the property WorkingSet greater than 10MB
> * use Get-Member to find a property that will show you the full path to the
>   executable file pwsh.exe (for the powershell process object)

Note: you should for efficiency reasons (and thereby climate change reasons)
filter objects as far left as you can, e.g. do `Get-Process -Name notepad`
instead of `Get-Process | Where-Object {$_.Name -eq 'notepad'}`

## Format-* Out-*

These cmdlets should always be at the end of the pipeline since they are used to
format/redirect the final output from the command pipeline. Remember this rule:
_filter to the left, format to the right_.

```powershell
Get-ChildItem -File C:\Windows\ | Format-List -Property Name,Length,LastAccessTime
Get-ChildItem -File C:\Windows\ | Format-Table -Property Name,Length,LastAccessTime
```

You can also send output from the pipeline to other places than the terminal
window (you can redirect output to a file with `>` just like in Linux).

```powershell
Get-ChildItem -File b* | Out-File b.dat          # is the same as > b.dat
Get-ChildItem -File b* | Out-GridView
Get-ChildItem -File b* | clip                    # send it to the clipboard
Get-ChildItem -File b* | Export-Csv $home\a.csv
```

> TAB through all `ConvertTo-` cmdlets

Note: sometimes when you try to output data, you can get strange results because
of how PowerShell behaves which is not always intuitive, e.g. PowerShell
automatically adds `Out-Default` to the end of every pipeline. See example when
[using semicolons](https://superuser.com/a/1564334) (same thing can happen
inside scripts/functions). 

## Sort-Object

You can use `Sort-Object` to sort on any property of an object, here are some
nice examples including a very practical use of `Get-Date` (note: a command line
can span several lines)

```powershell
Get-ChildItem C:\Windows\System32\ |
  Where-Object {$_.LastAccessTime -gt (Get-Date).AddMinutes(-30)} |
    Sort-Object -Property Length -Descending
Get-ChildItem C:\Windows\System32\ |
  Where-Object {$_.LastAccessTime -gt (Get-Date).AddMinutes(-30)} |
    Sort-Object -Property Length |
      Select-Object -Last 10
```

Notice the importance of time stamps. Time stamps are crucial in log files, in
backups, in all kinds of forensics investigations (building a timeline of events
on a computer system), but they are also just very practical when we want to
find which file we edited or downloaded last Thursday when we don't remember the
name.

> Do `cd $env:homepath`. List all files recursively (recursively means include
> all files in all subdirectories) in your home directory where the filename
> matches the regular expression `'.*\.[tpl].*'`. Sort the output on the
> property LastWriteTime. Pipe to `Format-Table` and show only the properties
> Name, LastWriteTime and Length.

## Measure-Object

You have already used `Measure-Command` to measure the time it takes to run a
pipeline. If you want to count characters, words, lines and compute some
statistics from these, then use `Measure-Object`.

```powershell
Get-ChildItem -File C:\Windows\System32\ | Measure-Object
Get-ChildItem -File C:\Windows\System32\ | Measure-Object -Property Length -Sum -Average
```

## ForEach-Object, foreach, %

You use `Where-Object` for filtering objects in the pipeline. You use
`ForEach-Object` when you want to do something (with some flexibility) to each
object in the pipeline. You can also use [more standard loops and
conditions](https://docs.microsoft.com/en-us/powershell/scripting/learn/ps101/06-flow-control).
You will typically use them if you create a script (which is just commands
placed in a file).

```powershell
foreach ($i in 1..4) {notepad}
Get-Process notepad | ForEach-Object {Write-Output "Terminating Notepad with id $($_.id)";$_.Kill()}
```

Note: if you just wanted to stop all notepad-processes you could simply pipe to
`Stop-Process`.

> Start a pipeline `'Set-Location', 'Get-Location', 'Push-Location' |` and use
> `ForEach-Object` so you can iterate over these three cmdlet-names and call
> `Get-Alias -Definition $_` for each of them

## if, comparison, tests, "branching"

Of course PowerShell also has [the
if-statement](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_if),
a few examples:

```powershell
# if the Administrator-user account is enabled
if ( (Get-LocalUser -Name Administrator).Enabled )
  { Write-Output "It's enabled!" }
# if a a string is not empty
if ( $name.Length -gt 0 ) { Write-Output "Name has characters" }
# if a file exists
if ( Test-Path C:\Windows\System32\ntoskrnl.exe ) 
  { Write-Output "OS exists!" }
```

Note that sometimes it is easier to use [Compare-Object](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/compare-object), e.g. to copy only new or newer files

```powershell
$src = Get-ChildItem -Recurse "C:\Users\Admin\test\"
$dst = Get-ChildItem -Recurse "C:\Backup\"
if ($src -eq $NULL -or $dst -eq $null) {
  Write-Output "Compare-Object does not like empty objects :)"
  return
}
Compare-Object -ReferenceObject $dst -DifferenceObject $src -Property LastWriteTime -PassThru |
 Where-Object {$_.SideIndicator -eq '=>'} |
 Copy-Item -Recurse -Force -Destination C:\Backup\
```

When using `Compare-Object` with the `-PassThru` option the property `SideIndicator` is added to the object in the pipeline and can be used to select objects accro

## Modules

We can extend PowerShells functionality with modules. E.g. if we want to manage Windows Update through PowerShell, we can install a module for it

```powershell
Find-Module *windowsupdate*
#  Run as administrator 
Install-Module -Name PSWindowsUpdate
Get-WindowsUpdate
Get-WindowsUpdate -Download
# Get-WindowsUpdate -Install # maybe not now, since slow
```

> `Get-WindowsUpdate` by default contacts "Windows update server" which only
> covers updates for the operating system. Can you use `Get-WindowsUpdate` to
> contact "Microsoft Update server" (updates for other Microsoft software)
> instead?

It is also nice to control fonts in PowerShell with the module WindowsConsoleFonts

```powershell
Install-Module -Name WindowsConsoleFonts -Force
Get-Command -Module WindowsConsoleFonts
Set-ConsoleFont -Size 28 Consolas
```

## Scripts

A [PowerShell-script is just a collection of commands in a
file](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_scripts).
To create a text file we can
* use a text editor (vscode, nano, vim, notepad, ...)
* use `Out-File` (`>`) with multiple additional `Out-File -Append` (`>>`)
* use a [here
  document](https://en.wikipedia.org/w/index.php?title=Here_document&oldid=985675265)
  (called a [here string in
  PowerShell](https://powershell.org/2019/04/hear-hear-for-here-strings/)) 
  
  For creating small files and showing exactly how we do it in this tutorial,
  the most practical is to use a here string, so let's create the script
  `myscript.ps1` where we also demonstrate how we can give our script a
  parameter

```powershell
@'
param([string]$Name)
"Hello $Name, the size of this file is in Bytes:"
(Get-ChildItem .\myscript.ps1).Length
'@ > myscript.ps1
```

> Create the script above, execute it with/without providing the parameter
> `Name` and with/without providing av value for the parameter `Name`

Sometimes you will see `&` as prefix to scripts or commands when you
TAB-completion (try to rename `myscript.ps1` to `my script.ps1` and use
TAB-completion to execute it). `&` is the call-operator, browse [about
Operators](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_operators)
to learn more.

Whenever we code, we should use available tools to check for bad code quality.
In PowerShell we can do this with
[PSScriptanalyzer](https://github.com/PowerShell/PSScriptAnalyzer) which is a
static code analyzer for PowerShell. For bigger projects one should look into
[Pester](https://github.com/pester/Pester).

```powershell
Install-Module -Name PSScriptAnalyzer
# use it on script myscript.ps1
Invoke-ScriptAnalyzer myscript.ps1
# no output is a good thing :)
```

# PowerShell 2: Domain-joined hosts and Remoting

We would like to manage remote hosts by connecting to them and executing
commands without using a full Remote Desktop Protocol (RDP) GUI-session. RDP
(port 3389) has been at the center of [some vulnerabilities in recent
years](https://msrc.microsoft.com/update-guide/vulnerability/CVE-2019-0708), and
automating management tasks is difficult with a GUI like RDP. With PowerShell
remoting you can execute commands or login (create a session) on remote hosts
[using either the WSMan or SSH
protocols](https://docs.microsoft.com/en-us/powershell/scripting/learn/remoting/running-remote-commands).
We will only focus on WSMan between Windows hosts. Let us examine how we can do
remoting in a couple of different scenarios where the client is a Windows 10
(the host we are connecting from) and the server is Server 2019 (the host we are
connecting to).

Note: We are now going to create a _Windows Domain_. This means that we have at
least one domain controller where the Active Directory service is installed, and
other client and server computers that have joined the domain by registering
them in Active Directory. Users can then log in to computers that are joined to
the domain with their user account in Active Directory (instead of a local user
account) and we can manage groups of computers more easily. Active Directory is
a core component in a Windows-based infrastructure and almost every company have
an Active Directory installed. As you can imagine, Active Directory is a highly
attractive target for attackers. A widely known incident (think also about how
many unknown incidents there are) is the attack on [Norsk Hydro in
2019](https://doublepulsar.com/how-lockergoga-took-down-hydro-ransomware-used-in-targeted-attacks-aimed-at-big-business-c666551f5880)
which had an [estimated cost of roughly NOK 800
million](https://www.dn.no/bors/hydro/brasil/norsk-hydro/hackerangrepet-mot-hydro-enda-dyrere-enn-tidligere-antatt-ny-prislapp-pa-800-millioner-kroner/2-1-898620#:~:text=Prislappen%20p%C3%A5%20det%20det%20omfattende,hovedkontoret%20i%20Oslo%20etter%20angrepet.)

### Prepare environment

This lab assumes you have a Windows 10 host called `cl1`, and two Windows Server
2019 called `dc1` and `srv1`. You can create this environment by following the
instructions [Basic Infrastructure
Orchestration](https://gitlab.com/erikhje/dcsg1005/-/blob/master/heat-labs.md)
with the Heat template
[cl_dc_srv_basic.yaml](https://gitlab.com/erikhje/heat-mono/-/blob/master/cl_dc_srv_basic.yaml).
We install the services Active Directory and DNS on `dc1`, join `cl1` to the
domain, while we leave `srv1` outside of the domain. We have chosen `sec.core`
as our domain name. Before we create a domain, you need to set a password for
the local Administrator user (who will become domain administrator) and you need
to have a password for safe mode. For simplicity we let these be the same
password (in production environments these should be unique of course). Choose a
password before moving on (by "Choose a password" we mean either to use your
password manager to generate a password for you or [create a password following
best practice](https://nettvett.no/passord/)). On `dc1` do (notice that since
`Install-ADDSForest` have many parameters, we use
[splatting](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_splatting))

```powershell
# run as administrator
Install-WindowsFeature AD-Domain-Services, DNS -IncludeManagementTools
$Password = Read-Host -Prompt 'Enter Password' -AsSecureString
Set-LocalUser -Password $Password Administrator
$Params = @{
    DomainMode                    = 'WinThreshold'
    DomainName                    = 'sec.core'
    DomainNetbiosName             = 'SEC'
    ForestMode                    = 'WinThreshold'
    InstallDns                    = $true
    NoRebootOnCompletion          = $true
    SafeModeAdministratorPassword = $Password
    Force                         = $true
}
Install-ADDSForest @Params
Restart-Computer
# Log in as SEC\Administrator with password from above, test our domain
Get-ADRootDSE
Get-ADForest
Get-ADDomain
# Any computers joined the domain?
Get-ADComputer -Filter *
```

On `cl1` (who will join the domain) the following _needs to be done in Windows
PowerShell_ (NOT in PowerShell Core) since `Add-Computer` is not supported in
PowerShell Core

```powershell
# run as administrator
Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses IP_ADDRESS_OF_DC1
$cred = Get-Credential -UserName 'SEC\Administrator' -Message 'Cred'
Add-Computer -Credential $cred -DomainName sec.core -PassThru -Verbose
Restart-Computer
```

Note: the following are just examples of typical scenarios and maybe not 100%
accurate, see [About Remote
Requirements](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_remote_requirements)
for details.

### Scenario 1: Both hosts in domain, logged in as Domain Administrator

In this scenario, remoting works by default (but you do not want to be be logged
in as domain administrator unless you really have to, think principle of least
privilege)

```powershell
Enter-PSSession dc1.sec.core
```

### Scenario 2: Both hosts in domain, logged in as local user

In this scenario, you just have to supply supply credentials for a domain user (who needs to be in domain-administrators or builtin-administrators group) on the remote host

```powershell
$cred = Get-Credential -Username SEC\Admin -Message 'Cred'
Enter-PSSession -Credential $cred dc1.sec.core
```

### Scenario 3: Connect to host outside of domain

In this scenario, you have to register `srv1` as a trusted host for `cl1`, supply credentials for the user on the remote host, and set a local policy on the remote host.

First, on `srv1` do

```powershell
New-ItemProperty -Name LocalAccountTokenFilterPolicy `
  -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System `
  -PropertyType DWord -Value 1
```

then, on `cl1`, you can do

```powershell
# check if any trusted hosts already added so we dont overwrite
$curValue = (Get-Item wsman:\localhost\Client\TrustedHosts).value
if ($curValue -eq '') { 
  Set-Item wsman:\localhost\Client\TrustedHosts -Value "IP_ADDRESS_OF_SRV1"
} else {
  Set-Item wsman:\localhost\Client\TrustedHosts -Value "$curValue, IP_ADDRESS_OF_SRV1"
}
$cred = Get-Credential -Username Admin -Message 'Cred'
Enter-PSSession -Credential $cred IP_ADDRESS_OF_SRV1
```

### Learning PowerShell Remoting through Practice

> Log in to `cl1` as `CL1\Admin`, read and do all the examples in the article
> [About
> Remote](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_remote).
> After doing this, you should be comfortable with `Enter-PSSession`,
> `New-PSSession` and `Invoke-Command`.

### Copying files with and without Shares

In the Linux world you always copy files with `scp` and you can use this
from Windows to a Linux host as well but the Windows-way of copying
files between hosts is using existing shares (this only works for writing
to domain controllers)

```powershell
Write-Output heisan > mysil.txt
Copy-Item mysil.txt \\dc1\C$\users\admin
Get-ChildItem \users\admin
Enter-PSSession dc1
Get-SmbShare
Get-ChildItem \users\admin
```

or creating temporary shares (a share is a network file system, in Windows
this is SMB (Server Message Block) sometimes called CIFS (Common Internet
File System))

```powershell
Enter-PSSession srv1
New-SmbShare -Name "admindocs$" -Path "C:\users/admin/documents" -Temporary -FullAccess "RESKIT\Administrator"
exit
Copy-Item mysil.txt \\srv1\admindocs$
Enter-PSSession srv1
Get-ChildItem C:\Users\Admin\Documents
Remove-SmbShare admindocs$
exit
``` 

> Do the `New-SmbShare` cmdlet above, but put all the arguments in a
> hashtable ([splatting](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_splatting))

It is possible to do something similar to scp in PowerShell by establishing
a remote session and then use `Copy-Item` with the `-ToSession` option

```powershell
$sess = New-PSSession srv1
Copy-Item .\mysil -ToSession $sess C:\users\admin
Get-PSSession
Remove-PSSession $sess
Get-PSSession
```

# Additional Resources

What seems to be one of the best resources for learning PowerShell is the
book [Don Jones and Jeffery Hicks, Learn Windows PowerShell in a Month of
Lunches](https://www.manning.com/books/learn-windows-powershell-in-a-month-of-lunches-third-edition)
which also have a accompanying [video
series](https://www.youtube.com/playlist?list=PL6D474E721138865A). A newer
and more comprehensive video series is [PowerShell Master
Class](https://www.youtube.com/playlist?list=PLlVtbbG169nFq_hR7FcMYg32xsSAObuq8). An
excellent resource for PowerShell remoting is [Secrets of PowerShell
Remoting](https://leanpub.com/secretsofpowershellremoting/read). A good introduction book is [PowerShell 101](https://docs.microsoft.com/en-us/powershell/scripting/learn/ps101/00-introduction).

# Appendix: Useful SysAdm cmdlets and commands

## System Info Cmdlets

```powershell
Get-Process
Get-Service
Get-ComputerInfo | Format-Table -AutoSize -Property WindowsVersion,CsPartOfDomain
Get-CimClass Win32*
Get-CimInstance Win32_ComputerSystem
```

## Networking Cmdlets

Useful networking cmdlets are

```powershell
Test-NetConnection
Get-NetAdapter
Get-NetConnectionProfile
Get-NetIPConfiguration
Get-NetTCPConnection
Get-NetRoute
Resolve-DnsName
Get-DnsClient
Get-DnsClientServerAddress
Get-DnsClientGlobalSetting
Clear-DnsClientCache
```
These also have corresponding "Set-" cmdlets, and some useful pipelines are
```powershell
Set-DnsClientGlobalSetting -SuffixSearchList @("node.consul")
Get-NetAdapter | Set-DnsClient -ConnectionSpecificSuffix "node.consul"
Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses IP_ADDRESS_OF_DNS_SERVER
Get-NetAdapter | Get-NetIPInterface -Addressfamily IPv4 | Set-NetIPInterface -DHCP Enabled
```

## Troubleshooting remoting

If remoting does not work, or if you are trying to set up remoting on hosts
that are not part of the same domain (or not joined to any domain), here
are som useful commands

```powershell
Get-Help about_remote_requirements -Online
Get-Service WinRM                          # Is the service running?
Get-NetTCPConnection -LocalPort 5985,5986  # Listening service on the right ports?
Test-NetConnection -Port 5985 IP_ADDRESS_OF_DC1
Test-WSMan
Test-WSMan -ComputerName srv2 -Authentication Default
Get-Item WSMan:\localhost\Client\TrustedHosts # Any trusted hosts?
# if Enable-PSRemoting -Force does not work try
winrm quickconfig
```

## Security notes

When studying kerberoasting, see [Decrypting the Selection of Supported Kerberos
Encryption
Types](https://techcommunity.microsoft.com/t5/core-infrastructure-and-security/decrypting-the-selection-of-supported-kerberos-encryption-types/ba-p/1628797).

```powershell
Get-ADComputer -Properties msDS-SupportedEncryptionTypes -filter *
```

## Mounting and Unmounting disks

If you attach a disk/volume/storage of some kind, you can do the following once
to make it usable as a drive

```powershell
# Find disk number:
Get-Disk
# Initialize
Initialize-Disk DISK_NUMBER
# Partition and format:
New-Partition -DiskNumber DISK_NUMBER -UseMaximumSize -AssignDriveLetter | Format-Volume
Get-PSDrive
```

After this you can remove it and reattach repeatedly like this (if Disk number
1, partition number 2 and mounted as drive letter `D:`):

```powershell
# "Unmount"
Get-Volume -Drive D | Get-Partition | Remove-PartitionAccessPath -AccessPath D:\
# "Mount"
Get-Disk -Number 1 | Get-Partition -PartitionNumber 2 | Add-PartitionAccessPath -AccessPath 'D:\'
```

## Scheduled Tasks

Adam Betram ("Adam the automator") has many useful blog posts on PowerShell, I
recommend [How to Set Up and Manage Scheduled Tasks with
PowerShell](https://adamtheautomator.com/powershell-scheduled-task/)

# Appendix: Other Useful Aspects

## Variable, Array and Hashtable

A variable can represent any kind of object, and a variable can be grouped
into an array or a hashtable. In an array, each element has a numeric
index. In a hashtable each element has a key as index (in other words, a
hashtable is a collection of key-value pairs).

```powershell
$a = 'hei'
$a.gettype()
$a = Get-ChildItem C:\Windows\system.ini
$a.gettype()
$a = @('frodeh','kelly')
$a.gettype()
$a[1]
$a
$a = @{'frodeh'='Frode Haug';'kelly'='Jia-Chun Lin'}
$a.gettype()
$a[1]
$a['kelly']
$a
$a.length
$a.keys
```

Useful Hashtable

```powershell
Get-ChildItem C:\Windows\System32\ntoskrnl.exe | Select-Object -Property Name,@{name='ReadableSize';expression={$_.Length/1MB}}
```

> Using the hashtable `$a` above (the last example), write a command
> line which will output "User frodeh has real name Frode Haug"

## Accessing .NET

Sometimes you will not find a cmdlet that does exactly what you need to
do. In those situations it is useful to know that through PowerShell you
have access to everything in .NET (PowerShell is built on top of
.NET). E.g. in the textbook Thomas Lee in one recipe writes *PowerShell's
certificate provider does not support copying a certificate into the root
CA store.  You can overcome this limitation by dipping down into the .NET
framework as shown in step 7*

You can access .NET-objects by starting a command with a bracket

```powershell
[math]::PI
2.3*4.5
[math]::Round(2.3*4.5)
Get-Date
[datetime]::Now
[datetime]::UtcNow
```

> Try `[System` and TAB your way through possible .NET-objects.

In PowerShell you use the `.`-operator when you access the properties and
methods of an object, e.g. `(Get-Process powershell).WorkingSet`. The
`::`-operator is used to access static properties and methods (properties
and methods that does not
belong to a specific instance of a class), you can view these with
`Get-Member` as well

```powershell
(Get-Date).GetType()
Get-Date | Get-Member
Get-Date | Get-Member -Static
[datetime] | Get-Member
[datetime] | Get-Member -Static
```

## Generating Passwords

Never generate a password yourself unless you know you really need to type it
manually several times (but if you really need to do that, create [a long
hard-to-brute-force-guess-password](https://docs.microsoft.com/en-us/microsoft-365/admin/misc/password-policy-recommendations),
also consider reading [936: Password
Strength](https://www.explainxkcd.com/wiki/index.php?title=936:_Password_Strength&oldid=201051)).
Always let a good local random password generator create passwords for services,
databases, accounts, cryptography, etc. This is easy to do in a script (do not
ask for passwords with `Read-Host` unless absolutely necessary). In Windows
PowerShell (5.1) you can do 

```powershell
Add-Type -AssemblyName 'System.Web'
[System.Web.Security.Membership]::GeneratePassword(20,0)
```

In PowerShell core this is not possible (since PowerShell core is based on .NET
core which has less functionality than the full .NET). You have to do something
like (note: `Get-Random` [is a good random number generator in PowerShell since
at least PowerShell
5.0](https://www.sans.org/blog/truerng-random-numbers-with-powershell-and-math-net-numerics))

```powershell
-join `
('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789 !"#$%&()*+,-./:;<=>?@[\]^_`{|}~'.ToCharArray() | 
 Get-Random -Count 20)

# or third party
Install-Module MlkPwgen
New-Password -Length 20
```
